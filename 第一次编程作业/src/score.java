import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
public class score {
 public static void main(String[] args) throws FileNotFoundException, IOException {
  // 初始化个人经验值
  int before = 0;
  int base = 0;
  int test = 0;
  int temp;
  // 读取配置文件
  Properties a = new Properties();
  a.load(new FileInputStream("../total.properties"));
  // 解析两个HTML文本
  File b = new File("../small.html");
  Document d = Jsoup.parse(b, "UTF-8");
  File c = new File("../all.html");
  Document e = Jsoup.parse(c, "UTF-8");
  // 云班课的经验统计综合
  if (d != null) {
   Elements elements = d.getElementsByAttributeValue("class", "interaction-row");
   for (int i = 0; i < elements.size(); i++) {
    if (elements.get(i).child(1).child(0).toString().contains("课前自测")) {
     if (elements.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
      Scanner sc = new Scanner(
        elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
      temp = sc.nextInt();
      before += temp;
     }}else if (elements.get(i).child(1).child(0).toString().contains("课堂小测")) {
     if (elements.get(i).child(1).child(2).toString().contains("已参与")) {
      Scanner sc = new Scanner(
        elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
      temp = sc.nextInt();
      test += temp;
     }}
    else if (elements.get(i).child(1).child(0).toString().contains("课堂完成")) {
     if (elements.get(i).child(1).child(2).toString().contains("已参与")) {
      Scanner sc = new Scanner(
        elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
      temp = sc.nextInt();
      base += temp;
     }}
     else if (elements.get(i).child(1).child(0).toString().contains("教学总结")) {
      if (elements.get(i).child(1).child(2).toString().contains("已参与")) {
       Scanner sc = new Scanner(
         elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
       temp = sc.nextInt();
       base += temp;
      }}else if (elements.get(i).child(1).child(0).toString().contains("结课")) {
       if (elements.get(i).child(1).child(2).toString().contains("已参与")) {
        Scanner sc = new Scanner(
          elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
        temp = sc.nextInt();
        base += temp;
       }}
   }
   if(e!=null) {
    Elements dlements = e.getElementsByAttributeValue("class", "interaction-row");
    for (int i = 0; i < dlements.size(); i++) {
     if (dlements.get(i).child(1).child(0).toString().contains("课前自测")) {
      if (dlements.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
       Scanner sc = new Scanner(
         dlements.get(i).child(1).child(2).children().get(0).children().get(10).text());
       temp = sc.nextInt();
       before += temp;
      }}
   }
  // 初始化个人经验值
  double befores = Integer.parseInt(a.getProperty("before"));
  double bases = Integer.parseInt(a.getProperty("base"));
  double tests = Integer.parseInt(a.getProperty("test"));
  //计算并输出最终成绩
  double myForere = (before / befores) * 100 * 0.25;
  double myBase = (base / bases) * 100 * 0.3 * 0.95;
  double myTest = (test / tests) * 100 * 0.2;
  double myScore = (myBase + myForere + myTest) * 0.9 + 6;
  System.out.println(String.format("%.2f", totalScore));
  }
 }
}}